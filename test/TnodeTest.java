
import static org.junit.Assert.*;
import org.junit.Test;

/** Testklass.
 * @author Jaanus
 */
public class TnodeTest {

   @Test (timeout=1000)
   public void testBuildFromRPN() { 
      String s = "1 2 +";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString().replaceAll("\\s+", "");
      assertEquals ("Tree: " + s, "+(1,2)", r);
      s = "2 1 - 4 * 6 3 / +";
      t = Tnode.buildFromRPN (s);
      System.out.println(t.getFirstChild());
      r = t.toString().replaceAll("\\s+", "");
      System.out.println(r);
      assertEquals ("Tree: " + s, "+(*(-(2,1),4),/(6,3))", r);
   }

   @Test (timeout=1000)
   public void testBuild2() {
      String s = "512 1 - 4 * -61 3 / +";
      Tnode t = Tnode.buildFromRPN (s);
      String r = t.toString().replaceAll("\\s+", "");
      assertEquals ("Tree: " + s, "+(*(-(512,1),4),/(-61,3))", r);
      s = "5";
      t = Tnode.buildFromRPN (s);
      r = t.toString().replaceAll("\\s+", "");
      assertEquals ("Tree: " + s, "5", r);
   }

   @Test (expected=RuntimeException.class)
   public void testEmpty1() {
      Tnode t = Tnode.buildFromRPN ("\t\t");
   }

   @Test (expected=RuntimeException.class)
   public void testEmpty2() {
      Tnode t = Tnode.buildFromRPN ("\t \t ");
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol() {
      Tnode t = Tnode.buildFromRPN ("2 xx");
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol2() {
      Tnode t = Tnode.buildFromRPN ("x");
   }

   @Test (expected=RuntimeException.class)
   public void testIllegalSymbol3() {
      Tnode t = Tnode.buildFromRPN ("2 1 + xx");
   }

   @Test (expected=RuntimeException.class)
   public void testTooManyNumbers() {
      Tnode root = Tnode.buildFromRPN ("2 3");
   }

   @Test (expected=RuntimeException.class)
   public void testTooManyNumbers2() {
      Tnode root = Tnode.buildFromRPN ("2 3 + 5");
   }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbers() {
      Tnode t = Tnode.buildFromRPN ("2 -");
   }

   @Test (expected=RuntimeException.class)
   public void testTooFewNumbers2() {
      Tnode t = Tnode.buildFromRPN ("2 5 + -");
   }

  @Test (expected=RuntimeException.class)
   public void testTooFewNumbers3() {
      Tnode t = Tnode.buildFromRPN ("+");
   }

   @Test (timeout=1000)
   public void test_SWAP_should_return_minus_5_2_when__given_2_5_SWAP_minus(){
      String given = "2 5 SWAP -";
      Tnode t = Tnode.buildFromRPN (given);
      String result = t.toString().replaceAll("\\s+", "");
      assertEquals ("Tree: " + given, "-(5,2)", result);
   }
   @Test (timeout=1000)
   public void test_1(){
      String given = "3 DUP *";
      Tnode t = Tnode.buildFromRPN (given);
      String result = t.toString().replaceAll("\\s+", "");
      assertEquals ("Tree: " + given, "*(3,3)", result);
   }
   @Test (timeout=1000)
   public void test_2(){
      String given = "2 5 9 ROT - +";
      Tnode t = Tnode.buildFromRPN (given);
      String result = t.toString().replaceAll("\\s+", "");
      assertEquals ("Tree: " + given, "+(5,-(9,2))", result);
   }
   @Test (timeout=1000)
   public void test_3(){
      String given = "2 5 9 ROT + SWAP -";
      Tnode t = Tnode.buildFromRPN (given);
      String result = t.toString().replaceAll("\\s+", "");
      assertEquals ("Tree: " + given, "-(+(9,2),5)", result);
   }
   @Test (timeout=1000)
   public void test_4(){
      String given = "2 5 DUP ROT - + DUP *";
      Tnode t = Tnode.buildFromRPN (given);
      String result = t.toString().replaceAll("\\s+", "");
      assertEquals ("Tree: " + given, "*(+(5,-(5,2)),+(5,-(5,2)))", result);
   }
   @Test (timeout=1000)
   public void test_5(){
      String given = "-3 -5 -7 ROT - SWAP DUP * +";
      Tnode t = Tnode.buildFromRPN (given);
      String result = t.toString().replaceAll("\\s+", "");
      assertEquals ("Tree: " + given, "+(-(-7,-3),*(-5,-5))", result);
   }
}

