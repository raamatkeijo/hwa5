import java.util.*;

/** Tree with two pointers.
 * @since 1.8
 */
public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;
   static String[] validOperations = {
           "+",
           "-",
           "*",
           "/"
   };

   public Tnode(String name){
      setName(name);
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Tnode getFirstChild() {
      return firstChild;
   }

   public Tnode getNextSibling() {
      return nextSibling;
   }

   public void setFirstChild(Tnode firstChild) {
      this.firstChild = firstChild;
   }

   public void setNextSibling(Tnode nextSibling) {
      this.nextSibling = nextSibling;
   }



   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      b.append(getName());
      if (isValidOperator(getName())){
         b.append("(");
         b.append(getFirstChild().toString());
         b.append(")");
         if (nextSibling != null){
            b.append(",");
            b.append(getNextSibling());
         }
      } else if (nextSibling != null){
         b.append(",");
         b.append(nextSibling);
      }
      return b.toString();
   }

   /**
    *
    * @param pol, equation in reversed polish notation
    * @return equation as a tree
    */
   public static Tnode buildFromRPN (String pol) {
      if (pol.isEmpty()){
         throw new RuntimeException("Panic! Got empty string as input.");
      }
      Tnode root = null;
      Tnode sibling = null;
      Tnode a = null;
      Tnode b = null;
      String[] pieces = chopString(pol);
      String invalidElement = validateElements(pieces);
      if (!Objects.equals(invalidElement, "")){
         throw new RuntimeException("Panic! Can't understand " + invalidElement + " in " + pol);
      }
      // Idea from https://www.geeksforgeeks.org/expression-tree/
      Stack<Tnode> tnodes = new Stack<>();
      for (String element:pieces) {
         if (isValidOperator(element)){
            root = new Tnode(element);
            b = tnodes.pop();
            a = tnodes.pop();
            root.setFirstChild(a);
            a.setNextSibling(b);
            tnodes.push(root);
         } else if (isValidNumericValue(element)){
            tnodes.push(new Tnode(element));
         } else if (!isValidOperator(element) || !isValidNumericValue(element)){
            throw new RuntimeException("Panic! Got unreadable string: " + pol);
         }
      }

      if (root == null && pieces.length >= 2){
         throw new RuntimeException("Panic! Could not find operator in: " + pol);
      }
      root = tnodes.pop();
      if (pieces.length >= 4 && !isValidOperator(root.name)){
         throw new RuntimeException("Panic! Got incorrect string: " + pol);
      }

      // TODO!!!
      return root;
   }

   private static String validateElements(String[] pieces){
      String invalidElement = "";
      for (String element : pieces){
         int counter = 0;
         if (!isValidOperator(element)) {
            counter++;

         } else if ( !isValidNumericValue(element)){
            counter++;
         }
         if (counter == 0){
            invalidElement = element;
         }
      }
      return invalidElement;
   }

   private static boolean isValidOperator(String s) {
      return Arrays.asList(validOperations).contains(s);
   }

   private static boolean isValidNumericValue(String s) {
      // https://www.baeldung.com/java-check-string-number
      if (s == null) {
         return false;
      }
      try {
         double d = Double.parseDouble(s);
      } catch (NumberFormatException nfe) {
         return false;
      }
      return true;
   }

   private static String[] chopString(String pol) {
      String[] pols = pol.split("\\s+");
      List<String> pieces = new ArrayList<>();
      for (String el : pols) {
         if (el.length() != 0) {
            pieces.add(el);
         }
      }
      return pieces.toArray(new String[0]);
   }

   public static void main (String[] param) {
      String rpn = "1 2 + 7 n";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      System.out.println(res.name);
      System.out.println(res.getFirstChild().name);
      System.out.println(res.getFirstChild().getNextSibling().name);
      // TODO!!! Your tests here
   }
}

